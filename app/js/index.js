'use strict';
/*jslint browser:true */

var menuDropdown = document.querySelectorAll('.js-categories__link');
for (var i = 0; i < menuDropdown.length; i++) {
	menuDropdown[i].onclick = function() {
		this.parentNode.querySelector('.categories__subcat').classList.toggle('open');
		return false;
	};
}

var modal = document.getElementById('myModal');
document.querySelector('.js-feedback__link').onclick = function () {
	modal.classList.toggle('show');
};
document.querySelector('.js-modal__close').onclick = function () {
	modal.classList.toggle('show');
};

window.onclick = function (event) {
	if (modal.classList.contains('show')) {
		if (event.target === modal) {
			modal.classList.toggle('show');
		}
	}
};

var inputs = document.querySelectorAll('.feedback-form__input');
for (var i = 0; i < inputs.length; i++) {
	inputs[i].onkeyup = function () {
		if (this.classList.contains('feedback-form__input--error')) {
			this.classList.remove('feedback-form__input--error');
			this.setAttribute('placeholder', '');
		}
	};
}

document.querySelector('.js-feedback-form__submit').onclick = function () {
		formValidation(this);
		var inputs = document.querySelectorAll('.feedback-form__input');
		var checker = true;
		for (var i=0; i < (inputs.length - 1); i++) {
			if (inputs[i].classList.contains('feedback-form__input--error')) {
				checker = false;
			}
		}
		if (checker) {
			modal.classList.toggle('show');
		}
};

function formValidation(t) {
	var checkVal = true;
	var field = document.querySelector('.feedback-form');
	//define error msg
	var msg = 'Поле обязательно для заполнения';

	//defire regexp
	var nameRe = /^([a-zа-я'"\-\s]{1,55})$/i;
	var emailRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var telRe = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

	//add error styles
	function addError (input) {
		if (!input.classList.contains('feedback-form__input--error')) {
			input.classList.add('feedback-form__input--error');
			input.setAttribute('placeholder', msg);
		}
	}

	var forms = field.getElementsByTagName('input');


	for(var it = 0; it < (forms.length - 1); it++) {
		if (!forms[it].value.length) {
			addError(forms[it]);
			checkVal = false;
		}
		if (forms[it].getAttribute == 'email') {
			 var cond = emailRe.test(forms[it].value);
			 if (!cond) {
					addError(forms[it]);
			 }
		}
		if (forms[it].getAttribute == 'telephone') {
			 var cond = telRe.test(forms[it].value);
			 if (!cond) {
					addError(forms[it]);
			 }
		}
		if (forms[it].getAttribute == 'name') {
			 var cond = nameRe.test(forms[it].value);
			 if (!cond) {
					addError(forms[it]);
			 }
		}
	}

}

/////////////////////////////////////
//slider
////////////////////////////////////


var slider = document.querySelector('.slider');
var sliderItems = document.querySelectorAll('.slider__item');
var prevButton = slider.querySelector('.slider__prev');
var nextButton = slider.querySelector('.slider__next');

// show prev slide
prevButton.onclick = function() {
	slideSelect('prev');
};

// show next slide
nextButton.onclick = function() {
	slideSelect('next');
};

function slideSelect (direction) {
	for (var i=0; i < sliderItems.length; i++) {
		if (sliderItems[i].classList.contains('is-visible')) {
			sliderItems[i].classList.remove('is-visible');
			if (direction === 'next') {
				if (i == (sliderItems.length - 1)) {
					sliderItems[0].classList.add('is-visible');
				} else {
					sliderItems[i + 1].classList.add('is-visible');
				}
			}
			if (direction === 'prev') {
				if (i === 0) {
					sliderItems[(sliderItems.length - 1)].classList.add('is-visible');
				} else {
					sliderItems[i - 1].classList.add('is-visible');
				}
			}

			break;
		}
	}
	dotsActive();
}

// initialize dots
function dotsInit () {
	var dotsAmount = sliderItems.length;
	var dotsContainer = slider.querySelector('.slider__dots');
	var dot = document.createElement('div');
	dot.className = 'slider__dots-item';
	for (var i=0; i < dotsAmount; i++) {
		if (dotsContainer.querySelector('.slider__dots-item') === null){
			dotsContainer.appendChild(dot);
		} else {
			var newDote = dotsContainer.querySelector('.slider__dots-item').cloneNode(true);
			dotsContainer.appendChild(newDote);
		}
	}
}
dotsInit();
// on dots click - show slide
var dots = slider.querySelectorAll('.slider__dots-item');
for (var i=0; i < dots.length; i++) {
	dots[i].onclick = function (event) {
		var index = indexInParent(this);
		if (!sliderItems[index].classList.contains('is-visible')) {
			slider.querySelector('.is-visible').classList.remove('is-visible');
			sliderItems[index].classList.add('is-visible');
		}
		dotsActive();
	};
}

function dotsActive () {
	var activeSlide = slider.querySelector('.slider__item.is-visible');
	var index = indexInParent(activeSlide);
	var activeDot = slider.querySelector('.slider__dots-item.active');
	if (activeDot === null) {
		slider.querySelector('.slider__dots-item').classList.add('active');
	}
	if (activeDot !== null) {
		activeDot.classList.remove('active');
	}
	if (!dots[index].classList.contains('active')) {
		dots[index].classList.add('active');
	}
}

// jquery index() analog
function indexInParent(node) {
    var children = node.parentNode.childNodes;
    var num = 0;
    for (var i=0; i<children.length; i++) {
         if (children[i]==node) return num;
         if (children[i].nodeType==1) num++;
    }
    return -1;
}


dotsActive();

window.onload = function () {

};

//////////////////////////
// slider with kind of OOP
/////////////////////////
/*
var slider2 = {
	init: function () {
		this.cacheDom();
    this.bindEvents();
		this.dotsInit();
	},
	cacheDom: function () {
		this.slider = document.querySelector('.slider');
		this.sliderItems = document.querySelectorAll('.slider__item');
		this.prevButton = this.slider.querySelector('.slider__prev');
		this.nextButton = this.slider.querySelector('.slider__next');
	},
	bindEvents: function () {
		this.prevButton.onclick = function() { slideSelect('prev'); };
		this.nextButton.onclick = function() { slideSelect('next'); };
		var dots = this.slider.querySelectorAll('.slider__dots-item');
		for (var i=0; i < dots.length; i++) {
			dots[i].onclick = function (event) {
				var index = indexInParent(this);
				if (!sliderItems[index].classList.contains('is-visible')) {
					slider.querySelector('.is-visible').classList.remove('is-visible');
					sliderItems[index].classList.add('is-visible');
				}
				dotsActive();
			};
		}
	}
	slideSelect: function (direction) {
		var sliderItems = this.sliderItems;
		for (var i=0; i < sliderItems.length; i++) {
			//find which slide is visible
			if (sliderItems[i].classList.contains('is-visible')) {
				// hide slide
				sliderItems[i].classList.remove('is-visible');
				// click on "next slider" arrow
				if (direction === 'next') {
					if (i == (sliderItems.length - 1)) {
						sliderItems[0].classList.add('is-visible');
					} else {
						sliderItems[i + 1].classList.add('is-visible');
					}
				}
				// click on "prev slider" arrow
				if (direction === 'prev') {
					if (i === 0) {
						sliderItems[(sliderItems.length - 1)].classList.add('is-visible');
					} else {
						sliderItems[i - 1].classList.add('is-visible');
					}
				}
				// break for loop
				break;
			}
		}
		dotsActive();
	}
	dotsInit: function () {
		var dotsAmount = this.sliderItems.length;
		var dotsContainer = slider.querySelector('.slider__dots');
		var dot = document.createElement('div');
		dot.className = 'slider__dots-item';
		for (var i=0; i < dotsAmount; i++) {
			if (dotsContainer.querySelector('.slider__dots-item') === null){
				dotsContainer.appendChild(dot);
			} else {
				var newDote = dotsContainer.querySelector('.slider__dots-item').cloneNode(true);
				dotsContainer.appendChild(newDote);
			}
		}
	},
	dotsActive: function () {
		var activeSlide = this.slider.querySelector('.slider__item.is-visible');
		var index = indexInParent(activeSlide);
		var activeDot = this.slider.querySelector('.slider__dots-item.active');
		var dots = slider.querySelectorAll('.slider__dots-item');
		if (activeDot === null) {
			this.slider.querySelector('.slider__dots-item').classList.add('active');
		}
		if (activeDot !== null) {
			activeDot.classList.remove('active');
		}
		if (!dots[index].classList.contains('active')) {
			dots[index].classList.add('active');
		}
	}
};
*/
